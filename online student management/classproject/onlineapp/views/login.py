from django.views import View

from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.models import User

from onlineapp.models import *
from django.urls import resolve
from django.shortcuts import  render,get_object_or_404,redirect
from onlineapp.forms.auth import *
def Logout(request):
    logout(request)
    return redirect('login_html')
class LoginView(View):
    def get(self,request,*args,**kwargs):
        if request.user.is_authenticated:
            return redirect('collegs_html')
        login=LoginForm()
        return render(request,template_name="login_page.html",context={'login':login})
    def post(self,request,*args,**kwargs):
        Login=LoginForm(request.POST)
        username=request.POST['username']
        password=request.POST['password']
        user=authenticate(request,username=username,password=password)
        if user is not None:
            login(request,user)
            return redirect('collegs_html')
        else:
            messages.error(request,'invalid crdentials')

        return render(request, template_name="login_page.html", context={'login': Login})
class SignUpView(View):
    def get(self,request,*args,**kwargs):
        signup=SignUpForm()
        return render(request,template_name='signup_page.html',context={'signup':signup})
    def post(self,request,*args,**kwargs):
        signup=SignUpForm(request.POST)
        username=request.POST['username']
        try:
            if User.objects.get(username=username):
                messages.error(request, 'user already exists')
                return redirect('signup_html')
        except:
            if signup.is_valid():
                user = User.objects.create_user(**signup.cleaned_data)
                user.save()
                if user is not None:
                    login(request,user)
                    return redirect('collegs_html')
                messages.error(request, 'signup failed')
                return redirect('signup_html')
